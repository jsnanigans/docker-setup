#!/bin/bash

# Based on https://gist.github.com/pkuczynski/8665367

parse_yaml() {
    local yaml_file=$1
    local prefix=$2
    local s
    local w
    local fs

    s='[[:space:]]*'
    w='[a-zA-Z0-9_.-]*'
    fs="$(echo @|tr @ '\034')"

    (
        sed -ne '/^--/s|--||g; s|\"|\\\"|g; s/\s*$//g;' \
            -e "/#.*[\"\']/!s| #.*||g; /^#/s|#.*||g;" \
            -e  "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
            -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" |

        awk -F"$fs" '{
            indent = length($1)/2;
            if (length($2) == 0) { conj[indent]="+";} else {conj[indent]="";}
            vname[indent] = $2;
            for (i in vname) {if (i > indent) {delete vname[i]}}
                if (length($3) > 0) {
                    vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
                    printf("%s%s%s%s=(\"%s\")\n", "'"$prefix"'",vn, $2, conj[indent-1],$3);
                }
            }' |

        sed -e 's/_=/+=/g' \
            -e '/\..*=/s|\.|_|' \
            -e '/\-.*=/s|\-|_|'

    ) < "$yaml_file"
}

create_variables() {
    local yaml_file="$1"
    eval "$(parse_yaml "$yaml_file")"
}


create_variables $(dirname $0)/vhosts.yml

for i in "${!vhosts__ServerName[@]}"
do
	php_network="php7:9000"

	if [ "${vhosts__php[$i]}" == "5" ]
	then
		php_network="php5:9000"
	fi

	echo "
		<VirtualHost *:80>
		    ServerName ${vhosts__ServerName[$i]}
		    ServerAlias ${vhosts__ServerAlias[$i]}

		    DocumentRoot \"/var/www/${vhosts__DocumentRoot[$i]}\"

		    <Directory \"/var/www/${vhosts__DocumentRoot[$i]}\">
		      Options Indexes FollowSymlinks MultiViews
		      AllowOverride All
		      Require all granted
		    </Directory>

		    <FilesMatch \"\.php$\">
		        # Require all granted
		        SetHandler proxy:fcgi://${php_network}
		    </FilesMatch>

		    ServerSignature Off
		    ErrorLog \"logs/${vhosts__ServerName[$i]}-error_log\"
		    CustomLog \"logs/${vhosts__ServerName[$i]}-access_log\" common
		    SetEnv LOOP_ENV development
		</VirtualHost>

		<VirtualHost *:443>
		    ServerName ${vhosts__ServerName[$i]}
		    ServerAlias ${vhosts__ServerAlias[$i]}

		    DocumentRoot \"/var/www/${vhosts__DocumentRoot[$i]}\"

		    <Directory \"/var/www/${vhosts__DocumentRoot[$i]}\">
		      Options Indexes FollowSymlinks MultiViews
		      AllowOverride All
		      Require all granted
		    </Directory>

		    <FilesMatch \"\.php$\">
		        # Require all granted
		        SetHandler proxy:fcgi://${php_network}
		    </FilesMatch>

		    ServerSignature Off
		    ErrorLog \"logs/${vhosts__ServerName[$i]}-error_log\"
		    CustomLog \"logs/${vhosts__ServerName[$i]}-access_log\" common
		    SetEnv LOOP_ENV development

		    SSLEngine on
		    SSLCertificateFile \"/usr/local/apache2/conf/server.crt\"
		    SSLCertificateKeyFile \"/usr/local/apache2/conf/server.key\"

		    <FilesMatch \"\.(cgi|shtml|phtml|php)$\">
		        SSLOptions +StdEnvVars
		    </FilesMatch>
		    <Directory \"/usr/local/apache2/cgi-bin\">
		        SSLOptions +StdEnvVars
		    </Directory>
		</VirtualHost>
	"
done

echo
